# coding=utf-8
"""
.. module:: Handlers.Ebooks
    :platform: Independent
    :synopsis: Module that will handle petitions for Ebooks content
"""
from BaseModules.Exceptions import PException
from BaseModules import Aspects
from Utilities import Utils, S3Handler
import requests
import json
import settings

license_service = None


def initialize_variables():
    globals()['license_service'] = settings.configuration().get('services').get('license')


class Ebooks(object):
    """
    .. class: Ebooks
    The handler class that is used to process a petition for an Ebooks content.
    """
    petition = None     #: Petition to be processed

    def __init__(self, petition):
        self.petition = petition

    @Aspects.exception_handler
    def check_order(self):
        """
        Method that consults if a link has been generated for a given transaction_id if
        it's provided, else it generates the transaction_id and returns False
        :return: Whether or not a link has already been generated for the transaction_id
        """
        if self.petition.content.transaction_id:
            headers = {'content-type': 'application/json'}
            params = {'web_service': 'Ebooks',
                      'web_method': 'check_order',
                      'transaction_id': self.petition.content.transaction_id}
            response = requests.post(license_service, data=json.dumps(params), headers=headers)
            if response.status_code == 200 and response.content:
                link = response.content.get('order_link')
                self._process_link(link)
                return True
            elif response.status_code == 400:
                raise PException(400, response.content)
            elif response.status_code == 200:
                raise PException(405, 'Error in response from Ebooks service.\n %d: %s' % (response.status_code,
                                                                                           response.content))
            return False
        else:
            self._create_transaction_id()
            return False

    @Aspects.exception_handler
    def get_license(self):
        """
        Method that asks for a license to a given product and provides the transaction_id
        for future reference.
        :return:
        """
        headers = {'content-type': 'application/json'}
        params = {'web_service': 'Ebooks',
                  'web_method': 'get_license',
                  'product_id': self.petition.content.proprietary_code,
                  'transaction_id': self.petition.content.transaction_id}
        response = requests.post(license_service, data=json.dumps(params), headers=headers)
        if response.status_code == 200 and response.content:
            link = response.content.get('order_link')
            self._process_link(link)
        elif response.status_code == 400:
            raise PException(400, response.content)
        else:
            raise PException(405, 'Error in response from Ebooks service.\n %d: %s' % (response.status_code,
                                                                                       response.content))

    def _process_link(self, link):
        """
        Given a link it process it and stores the link in the appropriate field in the
        petition class
        :param link: Link to the content requested
        :return:
        """
        content_type = Utils.obtain_link_type(link)
        if content_type in ['application/pdf', 'application/epub+zip']:
            path = Utils.obtain_s3_path(self.petition, content_type)
            self.petition.content.file_path = S3Handler.s3_upload_file(path, link, content_type)
            self.petition.content.token_path = None
            self.petition.status = settings.UPLOADED
        elif content_type == 'application/vnd.adobe.adept+xml':
            self.petition.content.token_path = link
            self.petition.content.file_path = None
            self.petition.status = settings.PENDING
        else:
            raise PException(400, 'File received was neither a PDF/EPUB nor an ACSM')

    def _create_transaction_id(self):
        """
        Method that creates a transaction_id for the given petition
        :return:
        """
        self.petition.content.transaction_id = '%s_%s_%s' % (self.petition.user.id, self.petition.id,
                                                             self.petition.content.proprietary_code[-5:])

