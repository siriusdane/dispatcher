# coding=utf-8
"""
.. module:: dispatcher.py
    :platform: Independent
    :synopsis: Main module of the project. Requests the petition to the Back-End service
    and then responds the processed petition to the respective service.
"""
from BaseModules.Petition import Petition
from BaseModules.Exceptions import PException
from BaseModules import Aspects
from Utilities import Utils
import requests
import urllib
import json
import signal
import threading
import sys
import settings

QUANTITY = 10   # Number of petitions to be requested to Back-End


# SERVICES
petition_service = None     #: Back-End service for processing petitions
brands_requested = None     #: Brands that the dispatcher will be requesting


def initialize_variables():
    """
    Function that initializes the global variables to the configuration
    :return:
    """
    globals()['petition_service'] = settings.configuration().get('services').get('petition')
    globals()['brands_requested'] = settings.configuration().get('brands')


# MAIN THREAD
# -----------
@Aspects.exception_handler
def service(thread_number):
    """
    This is the main process, it will be run with this file, initializes the configuration
    and starts the different threads that will be running and then waits for its finalization
    :param thread_number: Number of dispatcher threads that want to be run
    :return:
    """
    _configure_signals()
    settings.start_configuration()
    initialize_variables()
    status = threading.Thread(target=settings.update_statuses)
    status.daemon = True
    status.start()
    threads = [threading.Thread(target=dispatcher_thread) for _ in range(thread_number)]
    for thread in threads:
        thread.daemon = True
        thread.start()
    for thread in threads:
        thread.join()
    status.join()
    settings.mongodb_dropdb(settings.APPLICATION)


# MAIN SERVICES
# -------------
@Aspects.exception_handler
def dispatcher_thread():
    """
    Method that calls the dispatcher method for each of the brands configured,
    alternating between the statuses D and I
    :return:
    """
    status = [1 for _ in brands_requested]
    while settings.running():
        if not settings.reload_configuration():
            for index, brand in enumerate(brands_requested):
                if not dispatcher(settings.TO_PROCESS if status else settings.REPROCESS, brand):
                    status[index] ^= 1
        else:
            settings.load_configuration()


@Aspects.exception_handler
def dispatcher(status, brand):
    """
    Main function. Obtains the petitions, processes them and then responds to the
    Back-End service.
    :param status: Petition's status to be requested to the service
    :param brand: Petition's brand to be requested to the service
    :return: Whether or not there were any petitions in the response from the service
    """
    petitions = get_petitions(status, brand)
    for petition in petitions:
        response = process_petition(petition)
        respond_petition(response)
    return True if petitions else False


@Aspects.exception_handler
def get_petitions(status, brand):
    """
    Function that requests petitions from the Back-End service and instantiates them
    to Petition classes
    :param status: Petition's status to be requested to the service
    :param brand: Whether or not there were any petitions in the response from the service
    :return: List of petitions returned by the service
    """
    parameters = {'status': status, 'limit': QUANTITY, 'brand': brand}
    response = requests.get(petition_service + '?' + urllib.urlencode(parameters))
    return [Petition(e) for e in response.content]


@Aspects.exception_handler
def process_petition(petition):
    """
    Function that processes a petition.
    :param petition: Petition to be processed
    :return: A processed Petition
    """
    try:
        handler = Utils.obtain_issuer(petition)
        if not handler.check_order():
            handler.get_license()
        return handler.petition
    except PException as e:
        return e.process(petition)


@Aspects.exception_handler
def respond_petition(petition):
    """
    Function that returns a processed petition to the Back-End service. It loops for as
    long as it has to until Back-End acknowledges that it received it. And just for security
    measures it stores the petition in a database, and deletes it once it has been received
    by the Back-End service.
    :param petition: Petition to be sent
    :return:
    """
    petition_collection = 'failed_petitions'    # Collection to store petitions that are not received
    status, petition_id = False, None
    headers = {'content-type': 'application/json'}
    while not status:
        response = requests.post(petition_service, data=json.dumps(petition.to_dict()),
                                 headers=headers)
        status = True if response.status_code == 200 else False
        if not status and not petition_id:
            collection = settings.mongodb_connect(settings.APPLICATION, petition_collection)
            petition_id = collection.insert(petition.to_dict())
        elif status and petition_id:
            collection = settings.mongodb_connect(settings.APPLICATION, petition_collection)
            elem = collection.find_one({'_id': petition_id})
            elem.delete()


# AUXILIARY FUNCTIONS
# -------------------
@Aspects.exception_handler
def _configure_signals():
    """
    Method that configures the application to catch the interruption signals
    :return:
    """
    signal.signal(signal.SIGINT, _terminate)
    signal.signal(signal.SIGTERM, _terminate)


@Aspects.exception_handler
def _terminate(signum, frame):
    """
    When an interruption signal has been caught it tells the Database that the application
    is going to be stopped initializing the stoppage process.
    :param signum:
    :param frame:
    :return:
    """
    settings.set_application_status(False)


# EXECUTABLE
# ----------
if __name__ == '__main__':
    if len(sys.argv) == 2:
        settings.set_application_name(sys.argv[1])
        number = 1
    elif len(sys.argv) == 3:
        try:
            settings.set_application_name(sys.argv[1])
            number = int(sys.argv[2])
        except:
            print '[num_thread] argument must be an int.'
            sys.exit(0)
    else:
        print 'usage: python dispatcher.py [dispatcher_name] (num_thread)'
        print 'example: python dispatcher.py dispatcher_librero 5'
        sys.exit(0)
    service(number)