# coding=utf-8
"""
.. module:: BaseModules.Exceptions
    :platform: Independent
    :synopsis: Module that defines custom exceptions for the project
"""


class PException(Exception):
    def __init__(self, code=500, reason=None):
        self.code = code
        self.reason = reason

    def __str__(self):
        return self.reason

    def process(self, petition):
        if self.code == 400:
            petition.status = 'V'
        elif self.code == 404:
            petition.status = 'N'
        elif self.code == 405:
            petition.status = 'I'
        elif self.code == 500:
            petition.status = 'X'
        return petition

