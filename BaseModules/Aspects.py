# coding=utf-8
"""
.. module:: BaseModules.Aspects
    :platform: Independent
    :synopsis: Aspects to be placed in functions and methods across the project
"""
from BaseModules.Exceptions import PException
from BaseModules import Logger
import functools


def exception_handler(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            try:
                return function(*args, **kwargs)
            except PException:
                raise
            except Exception as e:
                raise PException(500, '%s %s' % (str(e), type(e)))
        except PException as e:
            Logger.insert_record(function.__module__, function.__name__, e.reason, 'ERROR')
    return wrapper


def method_logger(function):
    """ Logger aspect for the methods of a class
    """
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        import settings
        if settings.debug():
            message = 'INPUT: %s', ', '.join(str(arg) for arg in args)
            Logger.insert_record(function.__module__, function.__name__, message, 'INFO')
            result = function(*args, **kwargs)
            if result:
                message = 'OUTPUT: %s', result
            else:
                message = 'OUTPUT: %s', ', '.join(str(arg) for arg in args)
            Logger.insert_record(function.__module__, function.__name__, message, 'INFO')
        else:
            result = function(*args, **kwargs)
        return result
    return wrapper


def function_logger(function):
    """ Logger aspect for the functions that DO NOT belong to a class
    """
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        import settings
        if settings.debug():
            message = 'INPUT: %s', ', '.join(str(arg) for arg in args)
            Logger.insert_record(function.__module__, function.__name__, message, 'INFO')
            result = function(*args, **kwargs)
            if result:
                message = 'OUTPUT: %s', result
            else:
                message = 'OUTPUT: %s', ', '.join(str(arg) for arg in args)
            Logger.insert_record(function.__module__, function.__name__, message, 'INFO')
        else:
            result = function(*args, **kwargs)
        return result
    return wrapper